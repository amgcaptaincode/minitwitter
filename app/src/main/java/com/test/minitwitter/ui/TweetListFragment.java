package com.test.minitwitter.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.test.minitwitter.R;
import com.test.minitwitter.common.MyApp;
import com.test.minitwitter.data.TweetViewModel;
import com.test.minitwitter.network.MiniTwitterAuthClient;
import com.test.minitwitter.network.MiniTwitterAuthService;
import com.test.minitwitter.network.model.TweetResponse;

import java.util.List;


public class TweetListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private RecyclerView recyclerView;
    private MyTweetRecyclerViewAdapter adapter;
    private List<TweetResponse> tweetList;
    private TweetViewModel tweetViewModel;

    public TweetListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static TweetListFragment newInstance(int columnCount) {
        TweetListFragment fragment = new TweetListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tweetViewModel = ViewModelProviders.of(getActivity()).get(TweetViewModel.class);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tweet_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            adapter = new MyTweetRecyclerViewAdapter(getActivity(), tweetList);
            recyclerView.setAdapter(adapter);

            loadTweetData();
        }
        return view;
    }


    private void loadTweetData() {

        tweetViewModel.getTweets().observe(getActivity(), new Observer<List<TweetResponse>>() {
            @Override
            public void onChanged(List<TweetResponse> tweetResponses) {
                tweetList = tweetResponses;
                adapter.setData(tweetList);
            }
        });

    }


    private void showToast(String message) {
        Toast.makeText(MyApp.getContext(), message, Toast.LENGTH_SHORT).show();
    }

}
