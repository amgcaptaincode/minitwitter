package com.test.minitwitter.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.test.minitwitter.R;
import com.test.minitwitter.network.MiniTwitterClient;
import com.test.minitwitter.network.MiniTwitterService;
import com.test.minitwitter.network.model.AuthResponse;
import com.test.minitwitter.network.model.SingUpRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.test.minitwitter.common.ConstantsMT.CODE;
import static com.test.minitwitter.common.PreferencesManager.savedAuthResponse;

public class SingUpActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edUsername;
    private EditText edEmail;
    private EditText edPassword;
    private Button btnSingUp;
    private TextView tvBackLogin;

    private MiniTwitterClient miniTwitterClient;
    private MiniTwitterService miniTwitterService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        getSupportActionBar().hide();


        setupService();

        edUsername = findViewById(R.id.edUsername);
        edEmail = findViewById(R.id.edEmail);
        edPassword = findViewById(R.id.edPassword);
        btnSingUp = findViewById(R.id.btnSingUp);
        tvBackLogin = findViewById(R.id.tvBackLogin);
    }

    private void setupService() {
        miniTwitterClient = MiniTwitterClient.getInstance();
        miniTwitterService = miniTwitterClient.getMiniTwitterService();
    }

    private void signup() {
        String username = edUsername.getText().toString();
        String email = edEmail.getText().toString();
        String password = edPassword.getText().toString();

        if (TextUtils.isEmpty(username)) {
            edUsername.setError("El usuario es requerido");
        } else if (TextUtils.isEmpty(email)) {
            edEmail.setError("El email es requerido");
        } else if (TextUtils.isEmpty(password) || password.length() < 4) {
            edPassword.setError("La contraseña es requerida y minimo debe tener 4 caracteres");
        } else {
            SingUpRequest singUpRequest = new SingUpRequest(username, email, password, CODE);
            Call<AuthResponse> call = miniTwitterService.singUp(singUpRequest);
            call.enqueue(new Callback<AuthResponse>() {
                @Override
                public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                    if (response.isSuccessful()) {
                        savedAuthResponse(response.body());
                        showToast("Usuario creado correctamente.");
                        showDashBoard();
                    } else {
                        showToast("Error en los datos.");
                    }
                }

                @Override
                public void onFailure(Call<AuthResponse> call, Throwable t) {
                    showToast("Error en la conexión.");
                }
            });
        }

    }
    private void showDashBoard() {
        startActivity(new Intent(SingUpActivity.this, DashboardActivity.class));
        finish();
    }

    private void showToast(String message) {
        Toast.makeText(SingUpActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void backToLogin() {
        startActivity(new Intent(SingUpActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSingUp:
                signup();
                break;

            case R.id.tvBackLogin:
                backToLogin();
                break;

        }
    }
}
