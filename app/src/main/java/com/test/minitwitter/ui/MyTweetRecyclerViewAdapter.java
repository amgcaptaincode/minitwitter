package com.test.minitwitter.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.test.minitwitter.R;
import com.test.minitwitter.common.ConstantsMT;
import com.test.minitwitter.common.PreferencesManager;
import com.test.minitwitter.network.model.LikesItem;
import com.test.minitwitter.network.model.TweetResponse;

import java.util.List;

public class MyTweetRecyclerViewAdapter extends RecyclerView.Adapter<MyTweetRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<TweetResponse> mValues;

    public MyTweetRecyclerViewAdapter(Context context, List<TweetResponse> items) {
        this.context = context;
        this.mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_tweet, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        //holder.mItem = mValues.get(position);
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return mValues == null ? 0 : mValues.size();
    }

    public void setData(List<TweetResponse> tweetResponses) {
        this.mValues = tweetResponses;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView ivAvatar;
        public final ImageView ivLike;
        public final TextView tvUserName;
        public final TextView tvMessage;
        public final TextView tvLikesCount;
        public TweetResponse mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivAvatar = view.findViewById(R.id.ivAvatar);
            ivLike = view.findViewById(R.id.ivLike);
            tvUserName = view.findViewById(R.id.tvUsername);
            tvMessage = view.findViewById(R.id.tvMessage);
            tvLikesCount = view.findViewById(R.id.tvLikesCount);
        }

        public void onBind(int position) {
            mItem = mValues.get(position);

            tvUserName.setText(mItem.getUser().getUsername());
            tvMessage.setText(mItem.getMensaje());
            tvLikesCount.setText(String.valueOf(mItem.getLikes().size()));

            String photo = mItem.getUser().getPhotoUrl();
            if (!photo.equals("")) {
                Glide.with(context)
                        .load(String.format("https://www.minitwitter.com:3001/apiv1/uploads/photos/%s", photo))
                        .into(ivAvatar);
            }

            String userName = PreferencesManager.readPrefString(ConstantsMT.USERNAME_SP);

            for (LikesItem like : mItem.getLikes()) {
                if (like.getUsername().equals(userName)) {
                    Glide.with(context)
                            .load(R.drawable.ic_like_pink)
                            .into(ivLike);
                    tvLikesCount.setTextColor(context.getResources().getColor(R.color.pink));
                    tvLikesCount.setTypeface(null, Typeface.BOLD);
                    break;
                }
            }


        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvUserName.getText() + "'";
        }
    }
}
