package com.test.minitwitter.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.test.minitwitter.R;
import com.test.minitwitter.network.MiniTwitterClient;
import com.test.minitwitter.network.MiniTwitterService;
import com.test.minitwitter.network.model.AuthResponse;
import com.test.minitwitter.network.model.LoginRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.test.minitwitter.common.PreferencesManager.savedAuthResponse;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edEmail;
    private EditText edPassword;
    private Button btnLogin;
    private TextView tvSingUp;

    private MiniTwitterClient miniTwitterClient;
    private MiniTwitterService miniTwitterService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();


        setupService();

        edEmail = findViewById(R.id.edEmail);
        edPassword = findViewById(R.id.edPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvSingUp = findViewById(R.id.tvSingUp);


        btnLogin.setOnClickListener(this);
        tvSingUp.setOnClickListener(this);
    }

    private void setupService() {
        miniTwitterClient = MiniTwitterClient.getInstance();
        miniTwitterService = miniTwitterClient.getMiniTwitterService();
    }

    private void login() {
        String email = edEmail.getText().toString();
        String password = edPassword.getText().toString();

        if (TextUtils.isEmpty(email)) {
            edEmail.setError("El email es requerido");
        } else if (TextUtils.isEmpty(password) || password.length() < 4) {
            edPassword.setError("La constraseña es requerida y minimo debe tener 4 caracteres");
        } else {
            LoginRequest loginRequest = new LoginRequest(email, password);
            Call<AuthResponse> responseCall = miniTwitterService.login(loginRequest);
            responseCall.enqueue(new Callback<AuthResponse>() {
                @Override
                public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                    if (response.isSuccessful()) {
                        savedAuthResponse(response.body());
                        showToast("Loggeado correctamente");
                        showDashBoard();
                    } else {
                        showToast("Error en los datos");
                    }
                }

                @Override
                public void onFailure(Call<AuthResponse> call, Throwable t) {
                    showToast("Error en la conexión");
                }
            });
        }
    }

    private void showDashBoard() {
        startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
        finish();
    }

    private void showToast(String message) {
        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void goToSingUp() {
        startActivity(new Intent(LoginActivity.this, SingUpActivity.class));
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                login();
                break;

            case R.id.tvSingUp:
                goToSingUp();
                break;

        }
    }
}
