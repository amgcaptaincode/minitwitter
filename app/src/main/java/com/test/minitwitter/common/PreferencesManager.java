package com.test.minitwitter.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.test.minitwitter.network.model.AuthResponse;

import static com.test.minitwitter.common.ConstantsMT.ACTIVE_SP;
import static com.test.minitwitter.common.ConstantsMT.CREATED_SP;
import static com.test.minitwitter.common.ConstantsMT.EMAIL_SP;
import static com.test.minitwitter.common.ConstantsMT.MINITW_SP;
import static com.test.minitwitter.common.ConstantsMT.PHOTO_URL_SP;
import static com.test.minitwitter.common.ConstantsMT.TOKEN_SP;
import static com.test.minitwitter.common.ConstantsMT.USERNAME_SP;

public class PreferencesManager {

    public PreferencesManager() {
    }

    private static SharedPreferences getSharedPref() {
        return MyApp.getContext().getSharedPreferences(MINITW_SP, Context.MODE_PRIVATE);
    }

    public static void writePrefString(String key, String value) {
        SharedPreferences.Editor editor = getSharedPref().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void writePrefBoolean(String key, Boolean value) {
        SharedPreferences.Editor editor = getSharedPref().edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static String readPrefString(String key) {
        return getSharedPref().getString(key, null);
    }

    public static Boolean readPrefBoolean(String key) {
        return getSharedPref().getBoolean(key, false);
    }

    public static void savedAuthResponse(AuthResponse authResponse) {
        writePrefString(TOKEN_SP, authResponse.getToken());
        writePrefString(USERNAME_SP, authResponse.getUsername());
        writePrefString(EMAIL_SP, authResponse.getEmail());
        writePrefString(PHOTO_URL_SP, authResponse.getPhotoUrl());
        writePrefString(CREATED_SP, authResponse.getCreated());
        writePrefBoolean(ACTIVE_SP, authResponse.getActive());
    }
}
