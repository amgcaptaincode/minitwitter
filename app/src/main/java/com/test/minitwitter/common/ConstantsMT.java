package com.test.minitwitter.common;

public class ConstantsMT {

    public static final String BASE_URL = "https://www.minitwitter.com:3001/apiv1/";
    public static final String CODE = "UDEMYANDROID";
    public static final String MINITW_SP = "MINITW_SP";

    // PREFERENCES
    public static final String TOKEN_SP = "TOKEN_SP";
    public static final String USERNAME_SP = "USERNAME_SP";
    public static final String EMAIL_SP = "EMAIL_SP";
    public static final String PHOTO_URL_SP = "PHOTO_URL_SP";
    public static final String CREATED_SP = "CREATED_SP";
    public static final String ACTIVE_SP = "ACTIVE_SP";


}
