package com.test.minitwitter.network;

import com.test.minitwitter.network.model.TweetResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MiniTwitterAuthService {

    @GET("tweets/all")
    Call<List<TweetResponse>> getAllTweets();

}
