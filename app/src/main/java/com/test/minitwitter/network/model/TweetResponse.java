package com.test.minitwitter.network.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TweetResponse {

    @SerializedName("id")
    private int id;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("user")
    private User user;

    @SerializedName("likes")
    private List<LikesItem> likes;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setLikes(List<LikesItem> likes) {
        this.likes = likes;
    }

    public List<LikesItem> getLikes() {
        return likes;
    }
}