package com.test.minitwitter.network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.test.minitwitter.common.ConstantsMT.TOKEN_SP;
import static com.test.minitwitter.common.PreferencesManager.readPrefString;

public class MiniTwitterAuthInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        String token = readPrefString(TOKEN_SP);
        Request request = chain.request()
                .newBuilder()
                .addHeader("Authorization", String.format("Bearer %s", token)).build();
        return chain.proceed(request);
    }

}
