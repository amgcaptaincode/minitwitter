package com.test.minitwitter.network;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.test.minitwitter.common.ConstantsMT.BASE_URL;

public class MiniTwitterAuthClient {

    private static MiniTwitterAuthClient instance = null;
    private Retrofit retrofit = null;
    private MiniTwitterAuthService miniTwitterAuthService = null;

    public MiniTwitterAuthClient() {

        //Incluir en la cabecera de la peticion el TOKEn que autoriza al usuario

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(new MiniTwitterAuthInterceptor());
        OkHttpClient client = builder.build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        miniTwitterAuthService = retrofit.create(MiniTwitterAuthService.class);
    }

    public static MiniTwitterAuthClient getInstance() {
        if (instance == null) {
            instance = new MiniTwitterAuthClient();
        }
        return instance;
    }

    /*private Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }*/

    public MiniTwitterAuthService getMiniTwitterService() {
        return miniTwitterAuthService;
    }
}
