package com.test.minitwitter.network.model;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("photoUrl")
    private String photoUrl;

    @SerializedName("website")
    private String website;

    @SerializedName("created")
    private String created;

    @SerializedName("id")
    private int id;

    @SerializedName("username")
    private String username;

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getWebsite() {
        return website;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreated() {
        return created;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}