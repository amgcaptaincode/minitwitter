package com.test.minitwitter.network;

import com.test.minitwitter.network.model.AuthResponse;
import com.test.minitwitter.network.model.LoginRequest;
import com.test.minitwitter.network.model.SingUpRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface MiniTwitterService {

    @POST("auth/login")
    Call<AuthResponse> login(@Body LoginRequest loginRequest);

    @POST("auth/signup")
    Call<AuthResponse> singUp(@Body SingUpRequest singUpRequest);

}
