package com.test.minitwitter.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.test.minitwitter.common.ConstantsMT.BASE_URL;

public class MiniTwitterClient {

    private static MiniTwitterClient instance = null;
    private Retrofit retrofit = null;
    private MiniTwitterService miniTwitterService = null;

    public MiniTwitterClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        miniTwitterService = retrofit.create(MiniTwitterService.class);
    }

    public static MiniTwitterClient getInstance() {
        if (instance == null) {
            instance = new MiniTwitterClient();
        }
        return instance;
    }

    /*private Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }*/

    public MiniTwitterService getMiniTwitterService() {
        return miniTwitterService;
    }
}
