package com.test.minitwitter.data;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.test.minitwitter.common.MyApp;
import com.test.minitwitter.network.MiniTwitterAuthClient;
import com.test.minitwitter.network.MiniTwitterAuthService;
import com.test.minitwitter.network.model.TweetResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TweetRepository {

    private MiniTwitterAuthClient miniTwitterAuthClient;
    private MiniTwitterAuthService miniTwitterService;

    private LiveData<List<TweetResponse>> allTweets;

    public TweetRepository() {

        miniTwitterAuthClient = MiniTwitterAuthClient.getInstance();
        miniTwitterService = miniTwitterAuthClient.getMiniTwitterService();
        allTweets = getDataTweets();
    }

    public LiveData<List<TweetResponse>> getDataTweets() {
        final MutableLiveData<List<TweetResponse>> data = new MutableLiveData<>();

        Call<List<TweetResponse>> call = miniTwitterService.getAllTweets();
        call.enqueue(new Callback<List<TweetResponse>>() {
            @Override
            public void onResponse(Call<List<TweetResponse>> call, Response<List<TweetResponse>> response) {
                if (response.isSuccessful()) {
                    data.setValue(response.body());
                } else {
                    showToast("Algo ha ido mal");
                }
            }

            @Override
            public void onFailure(Call<List<TweetResponse>> call, Throwable t) {
                showToast("Error en la conexión");
            }
        });

        return data;
    }

    public LiveData<List<TweetResponse>> getAllTweets() {
        return allTweets;
    }

    private void showToast(String message) {
        Toast.makeText(MyApp.getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
